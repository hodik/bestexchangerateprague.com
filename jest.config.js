module.exports = {
  roots: ["<rootDir>/scraping"],
  transform: {
    "^.+\\.tsx?$": "ts-jest"
  },
  testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$",
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
  coveragePathIgnorePatterns: [
    "<rootDir>/node_modules/",
    ".*?/page-functions/.*?"
  ],
  coverageThreshold: {
    // This is a primitive check to ensure that every scraper file has been tested
    "**/sites/**/*.ts": {
      statements: 10
    }
  },
  collectCoverageFrom: [
    "**/*.{js,jsx}",
    "**/*.{ts,tsx}",
    "!**/node_modules/**",
    "!**/vendor/**"
  ]
};
