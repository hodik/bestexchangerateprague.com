import { createSitemap, EnumChangefreq, SitemapItemOptions } from "sitemap";
import { existsSync, mkdirSync, writeFileSync } from "fs";
import { dirname, resolve } from "path";
import { CURRENCY_PAGES } from "../src/api/currency-pages";

const sitemapXmlPath = resolve(process.cwd(), process.argv[2]);
const outputDir = dirname(sitemapXmlPath);
if (!existsSync(outputDir)) {
  // tslint:disable-next-line:no-console
  console.log(`Trying to create ${outputDir}...`);
  mkdirSync(outputDir, {
    recursive: true
  });
}

const urls: SitemapItemOptions[] = CURRENCY_PAGES.map(
  (page): SitemapItemOptions => ({
    url: page.path,
    changefreq: EnumChangefreq.HOURLY,
    priority: 0.8
  })
);

const sitemap = createSitemap({
  hostname: "https://bestexchangerateprague.com",
  // 600 sec cache period
  cacheTime: 600000,
  urls: [
    ...urls,
    {
      url: "/about",
      changefreq: EnumChangefreq.DAILY,
      priority: 0.5
    }
  ]
});

writeFileSync(sitemapXmlPath, sitemap.toString(), "utf-8");
