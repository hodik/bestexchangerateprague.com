import { TRANSLATION as RU_TRANSLATIONS } from "./ru";
import { TRANSLATION as UA_TRANSLATIONS } from "./ua";

export enum SupportedLocales {
  ru = "ru",
  en = "en",
  ua = "ua"
}

const LOCALES: { [index in SupportedLocales]?: { [id: string]: string } } = {
  [SupportedLocales.ru]: RU_TRANSLATIONS,
  [SupportedLocales.ua]: UA_TRANSLATIONS
};

export const i18nGet = (
  key: string,
  locale: SupportedLocales = SupportedLocales.en
): string => {
  const sanitizedKey = key.replace(/\n/g, "").replace(/([ ]+)/g, " ");
  const strings = LOCALES[locale];
  if (!strings) {
    return key;
  }
  return strings[sanitizedKey] ? strings[sanitizedKey] : key;
};
