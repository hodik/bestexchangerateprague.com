export const TRANSLATION = {
  "Rates for": "Курсы для",
  "Last updated": "Последнее обновление",
  "hours ago": "часов(-а) назад",
  Buy: "Покупка",
  Sell: "Продажа",
  Address: "Адрес",
  Exchange: "Обменник",
  Start: "Начать",

  "Our service helps find a currency exchange with the best rate for you in Prague. We regularly monitor multiple currencies across various exchanges and constantly update our database with fresh data.":
    "Наш сервис предназначен для поиска лучших обменных курсов в центре Праги. Мы регулярно сканируем большое количество сайтов обменников и всегда поддерживаем данные в актуальном состоянии",

  "Feel free to report about any discrepancy.":
    "Если вы обнаружили расхождение между нашими данными и реальными курсами, то можете сообщить через форму обратной связи",

  "DISCLAIMER: All information presented are merely as guidance, based on publicly available information, latest currency rates, and website metadatas. Check before deciding to buy or sell. This site is maintained and operated on voluntarily basis.":
    "ОТКАЗ ОТ ОТВЕТСТВЕННОСТИ: вся представленная информация, как общедоступная информация, свежие курсы валют и данные сайтов, является ориентировочной. Всегда проверяйте перед покупкой или продажей. Данный сайт поддерживается на добровольной основе.",

  "ALWAYS ASK HOW MANY CZECH CROWNS YOU WILL GET FOR THE AMOUNT YOU WANT TO CHANGE BEFORE YOU PUT THE MONEY INTO THE TRAY.":
    "ВСЕГДА УТОЧНЯЙТЕ, КАКУЮ ТОЧНУЮ СУММУ ВЫ ПОЛУЧИТЕ В КОНЕЧНОМ ИТОГЕ, ПЕРЕД ПЕРЕДАЧЕЙ ВАШИХ ДЕНЕГ."
};
