export const TRANSLATION = {
  "Rates for": "Курси для",
  "Last updated": "Оновлено",
  "hours ago": "годин(-а) тому",
  Buy: "Купівля",
  Sell: "Продаж",
  Address: "Адреса",
  Exchange: "Пункт обміну",
  Start: "Почати",

  "Our service helps find a currency exchange with the best rate for you in Prague. We regularly monitor multiple currencies across various exchanges and constantly update our database with fresh data.":
    "Наш сервіс допоможе вам знайти інформацію про найліпші курси валют в Празі. Ми постійно відслідковуємо велику кількість валют у різних пунктах обміну та постійно поповнюємо нашу базу актуальними даними.",

  "Feel free to report about any discrepancy.":
    "Не зволікайте повідомляти щодо будь яких розбіжностей",

  "DISCLAIMER: All information presented are merely as guidance, based on publicly available information, latest currency rates, and website metadatas. Check before deciding to buy or sell. This site is maintained and operated on voluntarily basis.":
    "ВІДМОВА ВІД ВІДПОВІДАЛЬНОСТІ: вся представлена інформация носить лише рекомендацій характер та заснована на загальнодоступній інформації, останніх курсах валют та метаданих веб-сайтів пунктів обміну. Перевіряйте, перш ніж вірішити щодо купівлі або продажу. Цей сайт підтримується та функціонує на добровільних засадах.",

  "ALWAYS ASK HOW MANY CZECH CROWNS YOU WILL GET FOR THE AMOUNT YOU WANT TO CHANGE BEFORE YOU PUT THE MONEY INTO THE TRAY.":
    "ЗАВЖДИ УТОЧНЮЙТЕ, СКІЛЬКИ ЧЕСЬКИХ КРОН ВИ ОТРИМАЄТЕ ЗА СУМУ ЯКУ ВИ ХОЧЕТЕ ОБМІНЯТИ ДО ТОГО ЯК ПОКЛАДЕТЕ ВАШІ ГРОШІ ДО ЛОТКУ ПУНКТУ ОБМІНУ"
};
