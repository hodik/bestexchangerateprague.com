import React, { useEffect, useState } from "react";
import {
  IonButtons,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonCol,
  IonContent,
  IonGrid,
  IonHeader,
  IonList,
  IonMenuButton,
  IonRow,
  IonSpinner,
  IonTitle,
  IonToolbar
} from "@ionic/react";
import { Currency, IExchange, IRatesInCity, ISellBuyPair } from "../types";
import { RouteComponentProps } from "react-router";
import "./RatesPage.css";
import { getData } from "../api/api";
import { i18nGet } from "../i18n";
import { trackPageview } from "../api/analytics";

export type IRatesPageProps = RouteComponentProps<{
  baseCurrency: string;
  targetCurrency: string;
  units: string;
}>;
interface ICardDetails {
  exchange: IExchange;
  rates: ISellBuyPair;
}

function getCards(rates: IRatesInCity, currency: Currency): ICardDetails[] {
  return rates.ratesInExchanges
    .map(data => ({
      exchange: data.exchange,
      rates: data.rates[currency]
    }))
    .filter(
      row =>
        row &&
        row.rates &&
        row.rates.buy &&
        row.rates.sell.rate &&
        row.rates.buy &&
        row.rates.buy.rate
    )

    .sort((a, b) => -a.rates.buy.rate + b.rates.buy.rate);
}

function getHowLongAgo(timestamp: Date): string {
  const hours = Math.floor(
    (new Date().getTime() - timestamp.getTime()) / 3600 / 1000
  );

  return hours < 1
    ? i18nGet("within our hour")
    : `${hours + 1} ${i18nGet("hours ago")}`;
}

export const RatesPage: React.FunctionComponent<IRatesPageProps> = ({
  match,
  history
}) => {
  trackPageview();

  const [cards, setCards] = useState<ICardDetails[]>([]);

  // const [centralBankRate, setCentralBankRate] = useState<ICurrencyRate>(
  //   undefined
  // );

  const [lastUpdate, setLastUpdate] = useState<Date>(undefined);

  const [loading, setLoading] = useState(true);

  const targetCurrency =
    Currency[
      match.params.targetCurrency.toUpperCase().trim() as keyof typeof Currency
    ];
  const units = parseInt(match.params.units, 10);

  useEffect(() => {
    getData().then(data => {
      setLastUpdate(new Date(data.lastUpdate));
      // setCentralBankRate(response.data.centralBankRates[currency].buy);
      setCards(getCards(data, targetCurrency));
      setLoading(false);
    });
  }, [targetCurrency]);

  return (
    <>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>

          <IonTitle>
            {units} {Currency[targetCurrency].toString().toUpperCase()}{" "}
            <div className="rates-sub-title">
              {i18nGet("Last updated")}:{" "}
              {lastUpdate
                ? getHowLongAgo(lastUpdate)
                : i18nGet("a long time ago")}
            </div>
          </IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        <IonGrid style={{ display: loading ? "block" : "none" }}>
          <IonRow>
            <IonCol />
            <IonCol style={{ textAlign: "center" }}>
              <IonSpinner name="dots" />
            </IonCol>
            <IonCol />
          </IonRow>
        </IonGrid>

        {cards ? (
          <IonList>
            {cards.map((card, index) => {
              const finalSumOnceBought = units * card.rates.buy.rate;
              // const finalSumOnceSold = units * card.rates.sell.rate;
              return (
                <IonCard
                  button
                  key={index}
                  onClick={() => {
                    history.push(
                      `/exchange/${encodeURIComponent(
                        card.exchange.id
                      )}?targetCurrency=${card.rates.buy.targetCurrency}`
                    );
                  }}
                >
                  <IonCardHeader>
                    <IonCardSubtitle>{card.exchange.name}</IonCardSubtitle>
                    <IonCardTitle>
                      {finalSumOnceBought.toFixed(4)}&nbsp;
                      {Currency[card.rates.buy.baseCurrency].toUpperCase()}
                    </IonCardTitle>
                  </IonCardHeader>

                  <IonCardContent>
                    <p>
                      {i18nGet("Buy")}: {card.rates.buy.rate.toFixed(4)}&nbsp;
                      {Currency[card.rates.buy.baseCurrency].toUpperCase()}
                    </p>

                    <p>
                      {i18nGet("Sell")}: {card.rates.sell.rate.toFixed(4)}
                      &nbsp;
                      {Currency[card.rates.sell.baseCurrency].toUpperCase()}
                    </p>

                    <p>
                      {i18nGet("Address")}: {card.exchange.address}{" "}
                    </p>
                  </IonCardContent>
                </IonCard>
              );
            })}
          </IonList>
        ) : null}
      </IonContent>
    </>
  );
};
