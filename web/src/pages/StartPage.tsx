import React from "react";
import {
  IonIcon,
  IonHeader,
  IonToolbar,
  IonButtons,
  IonButton,
  IonMenuButton,
  IonContent,
  IonTitle,
  IonText
} from "@ionic/react";
// tslint:disable-next-line:no-submodule-imports
import "./StartPage.css";
import { RouteComponentProps } from "react-router";
import { i18nGet } from "../i18n";
import { Disclaimer } from "../components/Disclaimer";
import { CURRENCY_PAGES } from "../api/currency-pages";
import { trackPageview } from "../api/analytics";

export const CurrencySelectorButton: React.FunctionComponent<{
  history: any;
  title: string;
  path: string;
}> = ({ history, title, path }) => {
  return <IonButton onClick={() => history.push(path)}>{title}</IonButton>;
};

export const StartPage: React.FunctionComponent<RouteComponentProps<{}>> = ({
  history
}) => {
  trackPageview();

  return (
    <>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonMenuButton></IonMenuButton>
          </IonButtons>
          <IonTitle>{i18nGet("Start")}</IonTitle>
          <IonButtons slot="end">
            <IonButton icon-only>
              <IonIcon slot="icon-only" name="more"></IonIcon>
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        {/*<div className="about-header">*/}
        {/*<img src="assets/img/ionic-logo-white.svg" alt="ionic logo" />*/}
        {/*</div>*/}
        <div className="ion-padding about-page">
          <p>
            {i18nGet(`Our service helps find a currency exchange with the best rate for
            you in Prague. We regularly monitor multiple currencies across
            various exchanges and constantly update our database with fresh
            data.`)}
          </p>

          <p></p>

          <IonText color="danger">
            {i18nGet(`ALWAYS ASK HOW MANY CZECH CROWNS YOU WILL GET FOR THE AMOUNT YOU WANT TO
            CHANGE BEFORE YOU PUT THE MONEY INTO THE TRAY.`)}
          </IonText>

          <p>{i18nGet(`Feel free to report about any discrepancy.`)}</p>

          <section className="currency-selector">
            {CURRENCY_PAGES.map((page, index) => (
              <div key={index}>
                <CurrencySelectorButton
                  history={history}
                  path={page.path}
                  title={page.title}
                />
              </div>
            ))}
          </section>

          <Disclaimer />
        </div>
      </IonContent>
    </>
  );
};
