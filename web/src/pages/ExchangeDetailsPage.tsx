import React, { useEffect, useState } from "react";

import {
  IonBackButton,
  IonButtons,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardTitle,
  IonCol,
  IonContent,
  IonGrid,
  IonHeader,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonNote,
  IonRow,
  IonSpinner,
  IonTitle,
  IonToolbar
} from "@ionic/react";
import { Currency, ICurrencyRates, IExchange, ISellBuyPair } from "../types";
import { RouteComponentProps } from "react-router";
import { getExchange, getExchangeRates } from "../api/api";
import { parse } from "query-string";
import { i18nGet } from "../i18n";
import { trackPageview } from "../api/analytics";

export type IExchangeDetailsProps = RouteComponentProps<{
  id: string;
}>;

/*
 * Return an array of sell/buy pairs so the the pair with `preferredCurrencyAtTheTop` as a target currency has been
 * put in the start of the array
 */
function getRatesArray(
  rates: ICurrencyRates,
  preferredCurrencyAtTheTop: Currency
): ISellBuyPair[] {
  const flatPairs: ISellBuyPair[] = Object.keys(rates).map(
    currency => rates[currency]
  );
  if (!preferredCurrencyAtTheTop) {
    return flatPairs;
  }
  const preferredPair = flatPairs.find(
    pair => pair.buy.targetCurrency === preferredCurrencyAtTheTop
  );
  if (!preferredPair) {
    return flatPairs;
  }

  return [preferredPair].concat(
    flatPairs.filter(
      pair => pair.sell.targetCurrency !== preferredCurrencyAtTheTop
    )
  );
}

export const ExchangeDetailsPage: React.FunctionComponent<
  IExchangeDetailsProps
> = ({ match, history, location }) => {
  trackPageview();

  const { id } = match.params;
  const exchangeId = decodeURIComponent(id);

  const [loading, setLoading] = useState(true);
  const [exchange, setExchange] = useState<IExchange>(undefined);
  const [rates, setRates] = useState<ICurrencyRates>(undefined);

  // Figure out if we need to put some ccy pairs in the top of the list
  const preferredCurrencyCandidate: string = parse(location.search)
    .targetCurrency as string;
  const preferredCurrency = preferredCurrencyCandidate
    ? Currency[
        preferredCurrencyCandidate.toUpperCase().trim() as keyof typeof Currency
      ]
    : undefined;

  // Load all details
  useEffect(() => {
    Promise.all([getExchange(exchangeId), getExchangeRates(exchangeId)]).then(
      ([exchangeRes, ratesRes]) => {
        setExchange(exchangeRes);
        setRates(ratesRes);
        setLoading(false);
      }
    );
  }, [exchangeId, preferredCurrency]);

  return (
    <>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonBackButton defaultHref="/" onClick={() => history.goBack()} />
          </IonButtons>

          <IonTitle>{i18nGet("Exchange")}</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent style={{ marginBottom: "30px" }}>
        <IonGrid style={{ display: loading ? "block" : "none" }}>
          <IonRow>
            <IonCol />
            <IonCol style={{ textAlign: "center" }}>
              <IonSpinner name="dots" />
            </IonCol>
            <IonCol />
          </IonRow>
        </IonGrid>

        {exchange ? (
          <>
            <IonCard>
              <IonCardHeader>
                <IonCardTitle>{exchange.name}</IonCardTitle>
              </IonCardHeader>
              <IonCardContent>
                <p>
                  {i18nGet("Website")}:{" "}
                  <a href={exchange.url} target="_blank">
                    {exchange.url}
                  </a>
                </p>

                <p>
                  {i18nGet("Address")}: {exchange.address}
                </p>
              </IonCardContent>
            </IonCard>

            <IonCard>
              <div className="gmap_canvas">
                <iframe
                  width="100%"
                  height="100%"
                  id="gmap_canvas"
                  src={`https://maps.google.com/maps?q=${exchange.address}&t=&z=15&ie=UTF8&iwloc=&output=embed`}
                  frameBorder="0"
                  scrolling="no"
                ></iframe>
              </div>
            </IonCard>
          </>
        ) : null}

        {rates ? (
          <IonList>
            <IonListHeader>Rates</IonListHeader>
            {getRatesArray(rates, preferredCurrency).map((pair, index) => {
              return (
                <div key={index}>
                  <IonItem
                    style={{ fontWeight: index === 0 ? "bolder" : "normal" }}
                  >
                    <IonLabel>
                      {i18nGet("Buy")}{" "}
                      {Currency[pair.buy.targetCurrency].toUpperCase()}
                    </IonLabel>
                    <IonNote slot="end" color="primary">
                      {pair.buy.rate.toFixed(4)}
                    </IonNote>
                  </IonItem>
                  <IonItem
                    style={{ fontWeight: index === 0 ? "bolder" : "normal" }}
                  >
                    <IonLabel>
                      {i18nGet("Sell")}{" "}
                      {Currency[pair.sell.targetCurrency].toUpperCase()}
                    </IonLabel>
                    <IonNote slot="end" color="primary">
                      {pair.sell.rate.toFixed(4)}
                    </IonNote>
                  </IonItem>
                </div>
              );
            })}
          </IonList>
        ) : null}
        <div style={{ height: "100px" }}>&nbsp;</div>
      </IonContent>
    </>
  );
};
