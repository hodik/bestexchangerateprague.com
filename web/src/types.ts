export enum Currency {
  CZK = "CZK",
  EUR = "EUR",
  USD = "USD",
  RUB = "RUB",
  UAH = "UAH",
  PLN = "PLN"
}

export enum CountryCode {
  CZECH_REPUBLIC = "cz"
}

export interface IRatesInExchange {
  exchange: IExchange;
  rates: ICurrencyRates;
}

export interface IRatesInCity {
  centralBankRates: ICurrencyRates;
  ratesInExchanges: IRatesInExchange[];
  lastUpdate: number;
}

export interface IExchange {
  id: string;
  name: string;
  address: string;
  url: string;
  getRatesFuncKey: string;
}

export interface ICentralBank {
  id: string;
  countryCode: string;
  getRatesFuncKey: string;
  name?: string;
  url?: string;
}

export enum Direction {
  SELL = "sell",
  BUY = "buy"
}

export interface ICurrencyRate {
  baseCurrency: Currency;
  targetCurrency: Currency;
  rate: number;
  direction: Direction;
  date: Date | string;
}

export interface ISellBuyPair {
  buy: ICurrencyRate;
  sell: ICurrencyRate;
}

export type ICurrencyRates = {
  [id in Currency]?: ISellBuyPair;
};
