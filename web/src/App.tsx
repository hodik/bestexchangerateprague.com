import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { Menu } from "./components/Menu";
import { IonApp, IonSplitPane, IonPage, IonReactRouter } from "@ionic/react";
// tslint:disable-next-line:no-submodule-imports
import "@ionic/core/css/core.css";
// tslint:disable-next-line:no-submodule-imports
import "@ionic/core/css/ionic.bundle.css";
import "./theme.css";

import { RatesPage } from "./pages/RatesPage";
import { ExchangeDetailsPage } from "./pages/ExchangeDetailsPage";
import { StartPage } from "./pages/StartPage";

export const App = () => {
  // const [locale, setLocale] = useState<SupportedLocales>(SupportedLocales.en);

  return (
    <IonApp>
      <IonReactRouter>
        <IonSplitPane contentId="main">
          <Menu />
          <IonPage id="main">
            <Switch>
              <Route
                path="/rates/:units/:targetCurrency/:baseCurrency"
                component={RatesPage}
              />
              <Route path="/exchange/:id" component={ExchangeDetailsPage} />
              <Route path="/about" component={StartPage} />

              <Redirect to="/about" />
            </Switch>
          </IonPage>
        </IonSplitPane>
      </IonReactRouter>
    </IonApp>
  );
};
