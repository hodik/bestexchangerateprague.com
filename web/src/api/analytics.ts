import "./gtag.d.ts";

export function trackPageview() {
  gtag("set", "page", location.pathname + location.search);
  gtag("send", "pageview");
}
