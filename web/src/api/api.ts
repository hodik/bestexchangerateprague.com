import { CountryCode, ICurrencyRates, IExchange, IRatesInCity } from "../types";
import axios from "axios";

const apiCache = new Map();

export async function getData(
  countryCode?: CountryCode,
  city?: string
): Promise<IRatesInCity> {
  const KEY = { countryCode, city };

  let data = apiCache.get(KEY) as IRatesInCity;
  if (!data) {
    data = (await axios.get<IRatesInCity>(
      "https://s3.amazonaws.com/www.bestexchangerate.in/api/cz/prague/rates.json"
    )).data;
    apiCache.set(KEY, data);
  }

  return data;
}

export async function getExchanges(
  countryCode?: CountryCode,
  city?: string
): Promise<IExchange[]> {
  const data = await getData(countryCode, city);
  return data.ratesInExchanges
    .filter(entry => entry.exchange && entry.exchange.id && entry.rates)
    .map(entry => entry.exchange);
}
export async function getExchange(exchangeId: string): Promise<IExchange> {
  const exchanges = await getExchanges();
  return exchanges.find(exchange => exchange.id === exchangeId);
}

export async function getExchangeRates(
  exchangeId: string
): Promise<ICurrencyRates> {
  const data = await getData();
  const entries = data.ratesInExchanges.filter(
    entry => entry.exchange && entry.rates
  );
  const result = entries.find(entry => entry.exchange.id === exchangeId);
  return result ? result.rates : undefined;
}
