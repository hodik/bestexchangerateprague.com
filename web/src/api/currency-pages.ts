export const CURRENCY_PAGES = [
  { title: "EUR", path: "/rates/1/eur/czk" },
  { title: "USD", path: "/rates/1/usd/czk" },
  { title: "RUB", path: "/rates/100/rub/czk" },
  { title: "UAH", path: "/rates/1/uah/czk" },
  { title: "PLN", path: "/rates/1/pln/czk" }
];
