import React from "react";
import "./Disclaimer.css";
import { i18nGet } from "../i18n";

export const Disclaimer: React.FunctionComponent = () => {
  return (
    <>
      <p className="disclaimer">
        {i18nGet(`DISCLAIMER: All information presented are merely as guidance, based on
        publicly available information, latest currency rates, and website
        metadatas. Check before deciding to buy or sell. This site is maintained
        and operated on voluntarily basis.`)}
      </p>
    </>
  );
};
