import {
  IonContent,
  IonHeader,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonMenu,
  IonMenuToggle,
  IonTitle,
  IonToolbar
} from "@ionic/react";
import React from "react";
import { RouteComponentProps, withRouter } from "react-router";
import { i18nGet, SupportedLocales } from "../i18n";
import * as H from "history";
import { CURRENCY_PAGES } from "../api/currency-pages";

const routes = {
  appPages: CURRENCY_PAGES
};

type IMenuProps = RouteComponentProps<{}> & {
  setLocale?: (local: SupportedLocales) => void;
};

interface IMenuEntryProps {
  path: string;
  history: H.History;
  title: string;
}
const MenuEntry: React.FunctionComponent<IMenuEntryProps> = ({
  path,
  title,
  history
}) => {
  return (
    <IonMenuToggle key={title} auto-hide="false">
      <IonItem button onClick={() => history.push(path)}>
        <IonLabel>{title}</IonLabel>
      </IonItem>
    </IonMenuToggle>
  );
};

const MenuImpl: React.SFC<IMenuProps> = ({ history }) => {
  function renderlistItems(list: any[]) {
    return list
      .filter(route => !!route.path)
      .map(p => (
        <MenuEntry
          key={p.title}
          history={history}
          title={p.title}
          path={p.path}
        />
      ));
  }

  return (
    <IonMenu contentId="main">
      <IonHeader>
        <IonToolbar>
          <IonTitle>Menu</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent class="outer-content">
        <IonList>
          <IonListHeader>{i18nGet("Rates for")}</IonListHeader>
          {renderlistItems(routes.appPages)}
        </IonList>

        <IonList>
          <IonListHeader>{i18nGet("Start")}</IonListHeader>

          <MenuEntry title={i18nGet("Start")} path="/about" history={history} />
        </IonList>
      </IonContent>
    </IonMenu>
  );
};

export const Menu = withRouter(MenuImpl);
