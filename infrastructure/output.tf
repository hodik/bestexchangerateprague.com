output "DNS_CONFIGURATION_INSTRUCTIONS_FOR_DISTRIBUTION_1" {
  value = "You may want to create a CNAME record for ${local.domain} to ${aws_cloudfront_distribution.distribution.domain_name}"
}

