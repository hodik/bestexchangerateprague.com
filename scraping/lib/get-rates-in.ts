import {
  CountryCode,
  Currency,
  IRatesInCity,
  IRatesInExchange
} from "../model/types";
import { getCentralBank } from "../db/central-banks";
import { ALL_GET_RATES_FUNCS } from "./all-get-rates-funcs";
import { getExchanges } from "../db/exchanges";

export async function getRatesIn(
  countryCode: CountryCode,
  city: string,
  currencies: Currency[]
): Promise<IRatesInCity> {
  const centralBank = await getCentralBank(CountryCode.CZECH_REPUBLIC);
  const centralBankRates = await ALL_GET_RATES_FUNCS[
    centralBank.getRatesFuncKey
  ](currencies);

  const exchangeRates: IRatesInExchange[] = [];
  const exchanges = await getExchanges(countryCode, city);

  // validate exchange configuration: there should be all functions
  // defined in `getRatesFuncKey`
  const missingGetRateFuncs = exchanges
    .map(exchange => exchange.getRatesFuncKey)
    .filter(
      getRatesFuncKey =>
        typeof ALL_GET_RATES_FUNCS[getRatesFuncKey] !== "function"
    );
  if (missingGetRateFuncs.length > 0) {
    throw new Error("No getRates func for " + missingGetRateFuncs.join(", "));
  }

  for (const exchange of exchanges) {
    const getRates = ALL_GET_RATES_FUNCS[exchange.getRatesFuncKey];
    if (!getRates) {
      throw new Error("No getRates func for " + exchange.name);
    }
    try {
      const rates = await getRates(currencies);
      exchangeRates.push({
        exchange,
        rates
      });
    } catch (err) {
      continue;
    }
  }

  return {
    centralBankRates,
    ratesInExchanges: exchangeRates,
    lastUpdate: new Date().getTime()
  };
}
