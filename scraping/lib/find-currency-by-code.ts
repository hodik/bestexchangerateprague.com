import { Currency } from "../model/types";

export function findCurrencyByCode(code: string): Currency | undefined {
  return Currency[code.toUpperCase().trim() as keyof typeof Currency];
}
