import { GetRatesFunc } from "../model/types";
import { getCnbCzRates } from "../scrapers/cz/sites/api/cnb.cz";
import { convertToSimpleFunction } from "../services/puppeteer-manager";
import { getCernaruzeExchangeCzRates } from "../scrapers/cz/sites/puppeteer/cernaruze-exchange.cz";
import { getKotvaEuroexchangeCzRates } from "../scrapers/cz/sites/puppeteer/eurochange.cz:kotva";
import { getExchangeCzRates } from "../scrapers/cz/sites/puppeteer/exchange.cz";
import { getExchange8CzRates } from "../scrapers/cz/sites/puppeteer/exchange8.cz";
import { getCZInterchangeEuRates } from "../scrapers/cz/sites/puppeteer/interchange.eu";
import { getJindrisskaExchangeCzRates } from "../scrapers/cz/sites/puppeteer/jindrisska-exchange.cz";
import { getNekazankaExchangeCzRates } from "../scrapers/cz/sites/puppeteer/nekazanka-exchange.cz";
import { getSmenarnaPrahaCzRates } from "../scrapers/cz/sites/puppeteer/smenarna-praha.cz";
import { getSmenarnaPraha1CzRates } from "../scrapers/cz/sites/puppeteer/smenarna-praha1.cz";
import { getTopExchangeCzRates } from "../scrapers/cz/sites/puppeteer/top-exchange.cz";

export const ALL_GET_RATES_FUNCS: { [id: string]: GetRatesFunc } = {
  "cnb.cz": getCnbCzRates,
  "cernaruze-exchange.cz": convertToSimpleFunction(getCernaruzeExchangeCzRates),
  "euroexchange.cz:kotva": convertToSimpleFunction(getKotvaEuroexchangeCzRates),
  "exchange.cz": convertToSimpleFunction(getExchangeCzRates),
  "exchange8.cz": convertToSimpleFunction(getExchange8CzRates),
  "interchange.eu:cz": convertToSimpleFunction(getCZInterchangeEuRates),
  "jindrisska-exchange.cz": convertToSimpleFunction(
    getJindrisskaExchangeCzRates
  ),
  "nekazanka-exchange.cz": convertToSimpleFunction(getNekazankaExchangeCzRates),
  "smenarna-praha.cz": convertToSimpleFunction(getSmenarnaPrahaCzRates),
  "smenarna-praha1.cz": convertToSimpleFunction(getSmenarnaPraha1CzRates),
  "top-exchange.cz": convertToSimpleFunction(getTopExchangeCzRates)
};
