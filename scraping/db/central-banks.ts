import { CountryCode, ICentralBank } from "../model/types";
import {
  CENTRAL_BANK_SPREADSHEET_HEADER_TO_FIELDS_MAP,
  CENTRAL_BANKS_SPREADSHEET_ID
} from "../configuration";
import { mapRawObjects, nestedArraysToObjects } from "./object-transformations";
import { getSpreadsheetContent } from "./spreadsheet";

export async function getCentralBank(
  countryCode: CountryCode
): Promise<ICentralBank> {
  const result = await getSpreadsheetContent(
    `https://spreadsheets.google.com/feeds/cells/${CENTRAL_BANKS_SPREADSHEET_ID}/1/public/values?alt=json`
  );

  const centralBanks = mapRawObjects<ICentralBank>(
    nestedArraysToObjects(result),
    CENTRAL_BANK_SPREADSHEET_HEADER_TO_FIELDS_MAP
  );

  return centralBanks[0];
}
