/* tslint:disable:no-string-literal */
import axios from "axios";

export interface ICell {
  ["gs$cell"]: {
    row: string;
    col: string;
  };
  content: {
    ["$t"]: string;
  };
}

export interface IPublishedGoogleSpreadsheet {
  feed: {
    ["gs$rowCount"]: {
      ["$t"]: string;
    };
    ["gs$colCount"]: {
      ["$t"]: string;
    };
    entry: ICell[];
  };
}

export async function getSpreadsheetContent(url: string): Promise<string[][]> {
  const spreadsheet = (await axios.get<IPublishedGoogleSpreadsheet>(url)).data;

  const colCount = parseInt(spreadsheet.feed["gs$colCount"]["$t"], 10);
  const rowCount = parseInt(spreadsheet.feed["gs$rowCount"]["$t"], 10);
  const values: string[][] = Array(rowCount).fill([]);

  for (let index = 0; index < rowCount; index++) {
    values[index] = Array(colCount).fill("");
  }

  spreadsheet.feed.entry.forEach(cell => {
    const rowIndex = parseInt(cell["gs$cell"].row, 10) - 1;
    const colIndex = parseInt(cell["gs$cell"].col, 10) - 1;

    values[rowIndex][colIndex] = cell.content["$t"];
  });

  return values;
}
