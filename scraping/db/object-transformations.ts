// tslint:disable-next-line:interface-over-type-literal
export type IRecord = { [id: string]: string };

// [
//   ["header1", "header2"],
//   ["value1_1", "value1_2"],
//   ["value2_1", "value2_2"]
// ]
// --->
// [
//   { header1: "value1_1", header2: "value1_2" },
//   { header1: "value2_1", header2: "value2_2" }
// ]
export function nestedArraysToObjects(rows: string[][]): IRecord[] {
  const header = rows[0];
  return rows.slice(1).map(row =>
    row.reduce(
      (obj, value, index): IRecord => {
        const key = header[index];
        obj[key] = value;
        return obj;
      },
      ({} as unknown) as IRecord
    )
  );
}

export function mapRawObjects<T>(
  records: IRecord[],
  fieldMap: { [id in keyof T]: string }
): T[] {
  return records.map(
    (record): T =>
      (Object.keys(fieldMap).reduce<IRecord>((obj, key) => {
        obj[key] = record[fieldMap[key as keyof T]];
        return obj;
      }, {}) as unknown) as T
  );
}
