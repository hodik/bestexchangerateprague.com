import { CountryCode, IExchange } from "../model/types";
import {
  EXCHANGE_SPREADSHEET_IDS,
  EXCHANGE_SPREADSHEET_HEADER_TO_FIELDS_MAP
} from "../configuration";
import { mapRawObjects, nestedArraysToObjects } from "./object-transformations";
import { getSpreadsheetContent } from "./spreadsheet";

export async function getExchanges(
  countryCode: CountryCode,
  city: string
): Promise<IExchange[]> {
  const result = await getSpreadsheetContent(
    `https://spreadsheets.google.com/feeds/cells/${EXCHANGE_SPREADSHEET_IDS[countryCode]}/1/public/values?alt=json`
  );

  return mapRawObjects<IExchange>(
    nestedArraysToObjects(result),
    EXCHANGE_SPREADSHEET_HEADER_TO_FIELDS_MAP
  ).filter(exchange => exchange.active.toLowerCase() === "yes");
}
