import { Page } from "puppeteer";
import * as admin from "firebase-admin";

export enum Currency {
  CZK = "CZK",
  EUR = "EUR",
  USD = "USD",
  RUB = "RUB",
  UAH = "UAH",
  PLN = "PLN"
}

export enum CountryCode {
  CZECH_REPUBLIC = "cz"
}

export type GetRatesFunc = (currencies: Currency[]) => Promise<ICurrencyRates>;
export type PuppeteerBasedGetRatesFunc = (
  page: Page,
  currencies: Currency[]
) => Promise<ICurrencyRates>;

export interface IRatesInExchange {
  exchange: IExchange;
  rates: ICurrencyRates;
}

export interface IRatesInCity {
  centralBankRates: ICurrencyRates;
  ratesInExchanges: IRatesInExchange[];
  lastUpdate: number;
}

export interface IExchange {
  id: string;
  active: string;
  name: string;
  address: string;
  url: string;
  getRatesFuncKey: string;
}

export interface ICentralBank {
  id: string;
  countryCode: string;
  getRatesFuncKey: string;
  name?: string;
  url?: string;
}

export enum Direction {
  SELL = "sell",
  BUY = "buy"
}

export interface ICurrencyRate {
  baseCurrency: Currency;
  targetCurrency: Currency;
  rate: number;
  direction: Direction;
  date: Date | string;
}

export type IFirebaseExchangeRate = ICurrencyRate & {
  timestamp: admin.firestore.Timestamp;
  exchange: FirebaseFirestore.DocumentReference;
};

export type IFirebaseCentralBankRate = ICurrencyRate & {
  timestamp: admin.firestore.Timestamp;
  centralBank: FirebaseFirestore.DocumentReference;
};

export interface ISellBuyPair {
  buy: ICurrencyRate;
  sell: ICurrencyRate;
}

export type ICurrencyRates = {
  [id in Currency]?: ISellBuyPair;
};
