export function splitIntoChunks<T>(array: T[], chunkSize: number): T[][] {
  if (array.length === 0) {
    return [];
  }

  const chunks: T[][] = [];
  let chunk: T[] = [array[0]];

  for (let index = 1; index < array.length; index++) {
    if (index % chunkSize === 0) {
      chunks.push(chunk);
      chunk = [];
    }

    chunk.push(array[index]);
  }

  chunks.push(chunk);

  return chunks;
}
