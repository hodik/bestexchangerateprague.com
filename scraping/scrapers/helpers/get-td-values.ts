import { Page } from "puppeteer";
import { getTdValuesPageFunction } from "./page-functions/get-td-values-page-functions";

export async function getTdValues(
  page: Page,
  tableSelector: string
): Promise<string[]> {
  return page.$eval<string[]>(tableSelector, getTdValuesPageFunction);
}
