import { Page } from "puppeteer";
import { getTdValuesByTrPageFunction } from "./page-functions/get-td-values-by-tr-page-functions";

export async function getTdValuesByTr(
  page: Page,
  tableSelector: string
): Promise<string[][]> {
  return page.$eval<string[][]>(tableSelector, getTdValuesByTrPageFunction);
}
