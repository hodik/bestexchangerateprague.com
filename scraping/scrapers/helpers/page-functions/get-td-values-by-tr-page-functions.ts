export function getTdValuesByTrPageFunction(element: Element): string[][] {
  const trs = element.querySelectorAll("tr");

  const rows: string[][] = [];
  // tslint:disable prefer-for-of
  for (let index = 0; index < trs.length; index++) {
    const tds = trs[index].querySelectorAll("td");
    const tdValues: string[] = [];

    // tslint:disable prefer-for-of
    for (let index2 = 0; index2 < tds.length; index2++) {
      tdValues.push(tds[index2].innerHTML);
    }

    rows.push(tdValues);
  }

  return rows;
}
