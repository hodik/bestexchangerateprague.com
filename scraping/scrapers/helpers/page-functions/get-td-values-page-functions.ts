export function getTdValuesPageFunction(element: Element): string[] {
  const tdValues = [];
  const tds = element.querySelectorAll("td");

  // tslint:disable prefer-for-of
  for (let index = 0; index < tds.length; index++) {
    const td = tds[index];
    tdValues.push(td.innerHTML);
  }

  return tdValues;
}
