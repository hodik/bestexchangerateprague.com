/*
 * A generic scraper for the network of exchanges of EXCHANGE s.r.o.
 */

import { Page } from "puppeteer";
import { Currency, Direction, ICurrencyRates } from "../../../model/types";
import { getTdValues } from "../../helpers/get-td-values";
import { splitIntoChunks } from "../../helpers/split-into-chunks";
import striptags = require("striptags");

export async function getRatesFrom(
  page: Page,
  currencies: Currency[],
  url: string,
  columns: number
): Promise<ICurrencyRates> {
  if (currencies.length === 0) {
    return {};
  }
  page.setViewport({ width: 1024, height: 768 });
  await page.goto(url, { waitUntil: "networkidle2" });

  // extract the rates table
  const tdValues = (await getTdValues(page, "table.kurzy")).map(value =>
    striptags(value)
  );

  const rows: string[][] = splitIntoChunks(tdValues, columns);
  const rates: ICurrencyRates = {};
  rows.forEach(([countryFlag, currencyUnits, buyRate, sellRate]) => {
    if (!currencyUnits || !buyRate || !sellRate) {
      return;
    }

    // extract a currency code and a number of unites
    const matches = currencyUnits.match(/^(\d+).*?([A-Z]{3})/);
    if (!matches) {
      return;
    }
    const code = matches[2].toUpperCase();
    const units = parseInt(matches[1], 10);

    const currency = Currency[code as keyof typeof Currency];
    if (!currencies.includes(currency)) {
      return;
    }

    rates[currency] = {
      sell: {
        baseCurrency: Currency.CZK,
        targetCurrency: currency,
        rate: parseFloat(sellRate) / units,
        direction: Direction.SELL,
        date: new Date()
      },
      buy: {
        baseCurrency: Currency.CZK,
        targetCurrency: currency,
        rate: parseFloat(buyRate) / units,
        direction: Direction.BUY,
        date: new Date()
      }
    };
  });

  return rates;
}
