import { Currency } from "../../../../model/types";

export const REQUIRED_CURRENCIES = [Currency.EUR, Currency.RUB, Currency.USD];
