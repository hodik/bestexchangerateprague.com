import { Currency, ICurrencyRates } from "../../../../model/types";

export function checkRates(
  rates: ICurrencyRates,
  requiredCurrencies: Currency[]
) {
  requiredCurrencies.forEach(currency => {
    expect(rates[currency].sell.rate).toBeGreaterThan(0.01);
    expect(rates[currency].buy.rate).toBeGreaterThan(0.01);
  });
}

export function checkExchangeFee(
  currencies: Currency[],
  exchangeRates: ICurrencyRates,
  centralBankRates: ICurrencyRates,
  maxDifference: number
) {
  currencies.forEach(currency => {
    const spread = Math.abs(
      exchangeRates[currency].sell.rate - centralBankRates[currency].buy.rate
    );
    const fee = spread / 2.0;

    // console.log(`${Currency[currency]}: central bank rate = ${centralBankRates[currency].sell.rate} exchange rate = ${ratesOfExchanges[currency].sell.rate}`)

    expect(
      Math.floor((fee / centralBankRates[currency].sell.rate) * 100)
    ).toBeLessThanOrEqual(maxDifference);
  });
}

export type TestGetRatesFunc = () => Promise<ICurrencyRates>;

export function commonTests(
  requiredCurrencies: Currency[],
  getExchangeRatesFunc: TestGetRatesFunc,
  getCentralBankRates: TestGetRatesFunc
) {
  let exchangeRates: ICurrencyRates;
  let centralBankRates: ICurrencyRates;

  beforeAll(async () => {
    exchangeRates = await getExchangeRatesFunc();
    centralBankRates = await getCentralBankRates();
  });

  requiredCurrencies.forEach(currency => {
    it(`should scrape exchange rates for ${Currency[currency]}`, () => {
      checkRates(exchangeRates, [currency]);
    });

    it(`should return rate for ${Currency[currency]} differing no more than 15 percent from the central bank rate`, async () => {
      checkExchangeFee([currency], exchangeRates, centralBankRates, 15);
    });

    it(`should return sell rate which is greater than a buy rate for ${Currency[currency]}`, () => {
      expect(
        exchangeRates[currency].sell.rate - exchangeRates[currency].buy.rate
      ).toBeGreaterThan(0.001);
    });
  });
}
