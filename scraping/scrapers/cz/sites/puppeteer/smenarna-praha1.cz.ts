/*
 * http://www.smenarna-praha1.cz/cz/ in the Czech Republic
 */
import { Page } from "puppeteer";
import { Currency, Direction, ICurrencyRates } from "../../../../model/types";
import { splitIntoChunks } from "../../../helpers/split-into-chunks";
import { getTdValues } from "../../../helpers/get-td-values";
import striptags = require("striptags");

export async function getSmenarnaPraha1CzRates(
  page: Page,
  currencies: Currency[]
): Promise<ICurrencyRates> {
  if (currencies.length === 0) {
    return {};
  }

  page.setViewport({ width: 1024, height: 768 });
  await page.goto("http://www.smenarna-praha1.cz/kurzy.php", {
    waitUntil: "networkidle2"
  });

  const tdValues = (await getTdValues(page, "table.tabulka")).map(value =>
    striptags(value)
  );

  const rows: string[][] = splitIntoChunks(tdValues, 6);
  const rates: ICurrencyRates = {};
  rows.forEach(([flag, name, code, unitsStr, buyRate, sellRate]) => {
    if (!name || !code || !buyRate || !unitsStr || !sellRate) {
      return;
    }

    const currency = Currency[code.toUpperCase() as keyof typeof Currency];
    if (!currencies.includes(currency)) {
      return;
    }

    const units = parseInt(unitsStr, 10);

    rates[currency] = {
      sell: {
        baseCurrency: Currency.CZK,
        targetCurrency: currency,
        rate: parseFloat(sellRate) / units,
        direction: Direction.SELL,
        date: new Date()
      },
      buy: {
        baseCurrency: Currency.CZK,
        targetCurrency: currency,
        rate: parseFloat(buyRate) / units,
        direction: Direction.BUY,
        date: new Date()
      }
    };
  });

  return rates;
}
