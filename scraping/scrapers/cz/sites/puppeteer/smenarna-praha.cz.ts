/*
 * http://smenarna-praha.cz/
 */
import { Page } from "puppeteer";
import { Currency, Direction, ICurrencyRates } from "../../../../model/types";
import striptags = require("striptags");
import { findCurrencyByCode } from "../../../../lib/find-currency-by-code";
import { getTdValuesByTr } from "../../../helpers/get-td-values-by-tr";

export async function getSmenarnaPrahaCzRates(
  page: Page,
  currencies: Currency[]
): Promise<ICurrencyRates> {
  if (currencies.length === 0) {
    return {};
  }

  page.setViewport({ width: 1024, height: 768 });
  await page.goto("http://smenarna-praha.cz/kurzy_en.php", {
    waitUntil: "networkidle2"
  });

  const rows: string[][] = (await getTdValuesByTr(page, "table.tabulka")).map(
    tds => tds.map(value => striptags(value))
  );

  const UNITS_AND_CODE_REGEXP = /(\d+).*?([A-Z]{3})/;
  return rows
    .filter(
      ([flag, name, unitsAndCode, buyRateStr, sellRateStr]) =>
        name &&
        unitsAndCode.match(UNITS_AND_CODE_REGEXP) &&
        buyRateStr &&
        sellRateStr
    )
    .map(([flag, name, unitsAndCode, buyRateStr, sellRateStr]) => ({
      units: parseInt(unitsAndCode.match(UNITS_AND_CODE_REGEXP)[1], 10),
      currency: findCurrencyByCode(
        unitsAndCode.match(UNITS_AND_CODE_REGEXP)[2]
      ),
      buyRate: parseFloat(buyRateStr.replace(/[^0-9.]/g, "")),
      sellRate: parseFloat(sellRateStr.replace(/[^0-9.]/g, ""))
    }))
    .filter(
      ({ currency, units, buyRate, sellRate }) =>
        currencies.includes(currency) &&
        units > 0 &&
        buyRate > 0 &&
        sellRate > 0
    )
    .reduce<ICurrencyRates>((rates, { currency, units, buyRate, sellRate }) => {
      rates[currency] = {
        sell: {
          baseCurrency: Currency.CZK,
          targetCurrency: currency,
          rate: sellRate / units,
          direction: Direction.SELL,
          date: new Date()
        },
        buy: {
          baseCurrency: Currency.CZK,
          targetCurrency: currency,
          rate: buyRate / units,
          direction: Direction.BUY,
          date: new Date()
        }
      };

      return rates;
    }, {});
}
