/*
 * http://www.nekazanka-exchange.cz/
 */
import { Currency, ICurrencyRates } from "../../../../model/types";
import { getRatesFrom } from "../../lib/exchange-sro";
import { Page } from "puppeteer";

export const getNekazankaExchangeCzRates = async (
  page: Page,
  currencies: Currency[]
): Promise<ICurrencyRates> =>
  getRatesFrom(page, currencies, "http://www.nekazanka-exchange.cz/", 7);
