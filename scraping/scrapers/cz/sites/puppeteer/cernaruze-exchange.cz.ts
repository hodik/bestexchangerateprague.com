import { Page } from "puppeteer";
import { Currency, ICurrencyRates } from "../../../../model/types";
import { getRatesFrom } from "../../lib/exchange-sro";

export const getCernaruzeExchangeCzRates = async (
  page: Page,
  currencies: Currency[]
): Promise<ICurrencyRates> =>
  getRatesFrom(page, currencies, "http://www.cernaruze-exchange.cz/", 6);
