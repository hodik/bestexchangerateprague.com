/*
 * https://jindrisska-exchange.cz in the Czech Republic
 */
import { Currency, ICurrencyRates } from "../../../../model/types";
import { getRatesFrom } from "../../lib/exchange-sro";
import { Page } from "puppeteer";

export const getJindrisskaExchangeCzRates = async (
  page: Page,
  currencies: Currency[]
): Promise<ICurrencyRates> =>
  getRatesFrom(page, currencies, "http://www.jindrisska-exchange.cz/", 6);
