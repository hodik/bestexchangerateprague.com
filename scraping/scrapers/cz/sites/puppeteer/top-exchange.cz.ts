import { Page } from "puppeteer";
import { Currency, Direction, ICurrencyRates } from "../../../../model/types";
import striptags = require("striptags");
import { findCurrencyByCode } from "../../../../lib/find-currency-by-code";
import { getTdValuesByTr } from "../../../helpers/get-td-values-by-tr";

export async function getTopExchangeCzRates(
  page: Page,
  currencies: Currency[]
): Promise<ICurrencyRates> {
  if (currencies.length === 0) {
    return {};
  }

  page.setViewport({ width: 1024, height: 768 });
  await page.goto("http://top-exchange.cz/", { waitUntil: "networkidle2" });

  const rows: string[][] = (await getTdValuesByTr(page, "table.tabulka")).map(
    row => row.map(value => striptags(value))
  );

  return rows
    .filter(
      ([flag, name, code, unitsStr, buyRateStr, sellRateStr]) =>
        code && unitsStr && buyRateStr && sellRateStr
    )
    .map(([flag, name, code, unitsStr, buyRateStr, sellRateStr]) => ({
      currency: findCurrencyByCode(code),
      units: parseInt(unitsStr, 10),
      buyRate: parseFloat(buyRateStr.replace(",", ".")),
      sellRate: parseFloat(sellRateStr.replace(",", "."))
    }))
    .filter(
      ({ currency, units, buyRate, sellRate }) =>
        currencies.includes(currency) &&
        units > 0 &&
        buyRate > 0 &&
        sellRate > 0
    )
    .reduce<ICurrencyRates>((rates, { currency, units, buyRate, sellRate }) => {
      rates[currency] = {
        sell: {
          baseCurrency: Currency.CZK,
          targetCurrency: currency,
          rate: sellRate / units,
          direction: Direction.SELL,
          date: new Date()
        },
        buy: {
          baseCurrency: Currency.CZK,
          targetCurrency: currency,
          rate: buyRate / units,
          direction: Direction.BUY,
          date: new Date()
        }
      };

      return rates;
    }, {});
}
