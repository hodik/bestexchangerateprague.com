import { commonTests } from "../../__fixtures__/helpers";
import { REQUIRED_CURRENCIES } from "../../__fixtures__/common";
import { getCZInterchangeEuRates } from "../interchange.eu";
import { getNekazankaExchangeCzRates } from "../nekazanka-exchange.cz";
import { getCernaruzeExchangeCzRates } from "../cernaruze-exchange.cz";
import { getJindrisskaExchangeCzRates } from "../jindrisska-exchange.cz";
import { getSmenarnaPraha1CzRates } from "../smenarna-praha1.cz";
import { getCnbCzRates } from "../../api/cnb.cz";
import puppeteer, { Browser } from "puppeteer";
import { ICurrencyRates } from "../../../../../model/types";
import { getKotvaEuroexchangeCzRates } from "../eurochange.cz:kotva";
import { getExchangeCzRates } from "../exchange.cz";
import { getExchange8CzRates } from "../exchange8.cz";
import { getSmenarnaPrahaCzRates } from "../smenarna-praha.cz";
import { getTopExchangeCzRates } from "../top-exchange.cz";

describe("Puppeteer-based scraping (CZ/Prague)", () => {
  let chrome: Browser;
  let cnbRates: ICurrencyRates;

  beforeAll(async () => {
    chrome = await puppeteer.launch();
    cnbRates = await getCnbCzRates();
  });

  afterAll(async () => {
    await chrome.close();
  });

  describe("http://www.cernaruze-exchange.cz/ (CZ/Prague)", () => {
    commonTests(
      REQUIRED_CURRENCIES,
      async () =>
        getCernaruzeExchangeCzRates(
          await chrome.newPage(),
          REQUIRED_CURRENCIES
        ),
      async () => cnbRates
    );
  });

  describe("http://www.interchange.eu (CZ/Prague)", () => {
    commonTests(
      REQUIRED_CURRENCIES,
      async () =>
        getCZInterchangeEuRates(await chrome.newPage(), REQUIRED_CURRENCIES),
      async () => cnbRates
    );
  });

  describe("http://www.nekazanka-exchange.cz/ (CZ/Prague)", () => {
    commonTests(
      REQUIRED_CURRENCIES,
      async () =>
        getNekazankaExchangeCzRates(
          await chrome.newPage(),
          REQUIRED_CURRENCIES
        ),
      async () => cnbRates
    );
  });

  describe("http://www.jindrisska-exchange.cz/ (CZ/Prague)", () => {
    commonTests(
      REQUIRED_CURRENCIES,
      async () =>
        getJindrisskaExchangeCzRates(
          await chrome.newPage(),
          REQUIRED_CURRENCIES
        ),
      async () => cnbRates
    );
  });

  describe("http://www.smenarna-praha1.cz/ (CZ/Prague)", () => {
    commonTests(
      REQUIRED_CURRENCIES,
      async () =>
        getSmenarnaPraha1CzRates(await chrome.newPage(), REQUIRED_CURRENCIES),
      async () => cnbRates
    );
  });

  describe("https://www.euroexchange.cz/smenarna-praha-kotvca/ (CZ/Prague)", () => {
    commonTests(
      REQUIRED_CURRENCIES,
      async () =>
        getKotvaEuroexchangeCzRates(
          await chrome.newPage(),
          REQUIRED_CURRENCIES
        ),
      async () => cnbRates
    );
  });

  describe("http://www.exchange.cz/ (CZ/Prague)", () => {
    commonTests(
      REQUIRED_CURRENCIES,
      async () =>
        getExchangeCzRates(await chrome.newPage(), REQUIRED_CURRENCIES),
      async () => cnbRates
    );
  });

  describe("https://www.exchange8.cz/ (CZ/Prague)", () => {
    commonTests(
      REQUIRED_CURRENCIES,
      async () =>
        getExchange8CzRates(await chrome.newPage(), REQUIRED_CURRENCIES),
      async () => cnbRates
    );
  });

  describe("http://smenarna-praha.cz/en/home/ (CZ/Prague)", () => {
    commonTests(
      REQUIRED_CURRENCIES,
      async () =>
        getSmenarnaPrahaCzRates(await chrome.newPage(), REQUIRED_CURRENCIES),
      async () => cnbRates
    );
  });

  describe("http://smenarna-praha1.cz/en/home/ (CZ/Prague)", () => {
    commonTests(
      REQUIRED_CURRENCIES,
      async () =>
        getSmenarnaPraha1CzRates(await chrome.newPage(), REQUIRED_CURRENCIES),
      async () => cnbRates
    );
  });

  describe("http://top-exchange.cz/ (CZ/Prague)", () => {
    commonTests(
      REQUIRED_CURRENCIES,
      async () =>
        getTopExchangeCzRates(await chrome.newPage(), REQUIRED_CURRENCIES),
      async () => cnbRates
    );
  });
});
