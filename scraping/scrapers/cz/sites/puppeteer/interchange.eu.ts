/*
 * https://interchange.eu (CZ)
 */
import { Page } from "puppeteer";
import { Currency, Direction, ICurrencyRates } from "../../../../model/types";
import { splitIntoChunks } from "../../../helpers/split-into-chunks";
import { getTdValues } from "../../../helpers/get-td-values";
import striptags = require("striptags");

export async function getCZInterchangeEuRates(
  page: Page,
  currencies: Currency[]
): Promise<ICurrencyRates> {
  if (currencies.length === 0) {
    return {};
  }

  page.setViewport({ width: 1024, height: 768 });
  await page.goto(
    "https://calculator.interchange.eu/?country=cz&lng=english&transparent=true&ga=",
    { waitUntil: "networkidle2" }
  );

  const tdValues = (await getTdValues(page, "table.rates")).map(value =>
    striptags(value)
  );

  const rows: string[][] = splitIntoChunks(tdValues, 4);
  const rates: ICurrencyRates = {};
  rows.forEach(([name, code, buyRate, sellRate]) => {
    if (!name || !code || !buyRate || !sellRate) {
      return;
    }

    const currency = Currency[code.toUpperCase() as keyof typeof Currency];
    if (!currencies.includes(currency)) {
      return;
    }

    const units = currency === Currency.RUB ? 100 : 1;

    rates[currency] = {
      sell: {
        baseCurrency: Currency.CZK,
        targetCurrency: currency,
        rate: parseFloat(sellRate) / units,
        direction: Direction.SELL,
        date: new Date()
      },
      buy: {
        baseCurrency: Currency.CZK,
        targetCurrency: currency,
        rate: parseFloat(buyRate) / units,
        direction: Direction.BUY,
        date: new Date()
      }
    };
  });

  return rates;
}
