import { Page } from "puppeteer";
import { Currency, Direction, ICurrencyRates } from "../../../../model/types";
import { splitIntoChunks } from "../../../helpers/split-into-chunks";
import { getTdValues } from "../../../helpers/get-td-values";
import striptags = require("striptags");

async function getRatesFrom(
  url: string,
  page: Page,
  currencies: Currency[]
): Promise<ICurrencyRates> {
  if (currencies.length === 0) {
    return {};
  }

  page.setViewport({ width: 1024, height: 768 });
  await page.goto(url, { waitUntil: "networkidle2" });

  const tdValues = (await getTdValues(page, "table")).map(value =>
    striptags(value)
  );

  const rows: string[][] = splitIntoChunks(tdValues, 6);
  const rates: ICurrencyRates = {};
  rows.forEach(([code, unitsStr, buyRateStr, sellRateStr]) => {
    if (!code || !unitsStr || !buyRateStr || !sellRateStr) {
      return;
    }

    const currency =
      Currency[code.toUpperCase().trim() as keyof typeof Currency];

    if (!currencies.includes(currency)) {
      return;
    }

    const buyRate = parseFloat(buyRateStr.replace(",", "."));
    if (isNaN(buyRate)) {
      return;
    }

    const sellRate = parseFloat(sellRateStr.replace(",", "."));
    if (isNaN(buyRate)) {
      return;
    }

    const units = parseInt(unitsStr, 10);
    if (isNaN(units)) {
      return;
    }

    rates[currency] = {
      sell: {
        baseCurrency: Currency.CZK,
        targetCurrency: currency,
        rate: sellRate / units,
        direction: Direction.SELL,
        date: new Date()
      },
      buy: {
        baseCurrency: Currency.CZK,
        targetCurrency: currency,
        rate: buyRate / units,
        direction: Direction.BUY,
        date: new Date()
      }
    };
  });

  return rates;
}

export const getKotvaEuroexchangeCzRates = (
  page: Page,
  currencies: Currency[]
) =>
  getRatesFrom(
    "https://www.euroexchange.cz/tisk/kurzovni-listek/smenarna-praha-kotva/",
    page,
    currencies
  );
