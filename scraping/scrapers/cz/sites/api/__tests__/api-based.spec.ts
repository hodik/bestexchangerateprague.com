import { getCnbCzRates } from "../cnb.cz";
import { checkRates } from "../../__fixtures__/helpers";
import { REQUIRED_CURRENCIES } from "../../__fixtures__/common";

describe("http://www.cnb.cz/ (CZ/Prague)", () => {
  it("should scrape major currencies", async () => {
    checkRates(await getCnbCzRates(), REQUIRED_CURRENCIES);
  });

  it("should return the same sell and buy rates", async () => {
    const rates = await getCnbCzRates();

    REQUIRED_CURRENCIES.forEach(currency => {
      expect(rates[currency].sell.rate).toBe(rates[currency].buy.rate);
    });
  });
});
