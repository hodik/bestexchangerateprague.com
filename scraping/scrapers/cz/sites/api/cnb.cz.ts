/*
 * Czech Central Bank (https://www.cnb.cz)
 */
import axios from "axios";
import {
  Currency,
  Direction,
  ICurrencyRates
} from "../../../../../scraping/model/types";

export async function getCnbCzRates(): Promise<ICurrencyRates> {
  const result = await axios.get(
    "https://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.txt"
  );

  // leave only rates
  // skip two lines
  // 17.07.2019 #136
  // země|měna|množství|kód|kurz
  const rows: string[] = result.data.split("\n").slice(2);

  // split into tokens
  const tokensByRow = rows.map(row => row.split("|"));
  return tokensByRow.reduce<ICurrencyRates>((rates, tokens): ICurrencyRates => {
    const [countryStr, currencyNameStr, unitsStr, codeStr, rateStr] = tokens;
    if (!countryStr || !currencyNameStr || !unitsStr || !codeStr || !rateStr) {
      return rates;
    }

    const currency = Currency[codeStr.toUpperCase() as keyof typeof Currency];
    if (!currency) {
      return rates;
    }

    const units = parseInt(unitsStr, 10);
    if (isNaN(units) || units < 1) {
      return rates;
    }

    const rate = parseFloat(rateStr.replace(",", "."));
    if (isNaN(rate) || rate < 0) {
      return rates;
    }

    rates[currency] = {
      sell: {
        baseCurrency: Currency.CZK,
        targetCurrency: currency,
        rate: rate / units,
        direction: Direction.SELL,
        date: new Date()
      },

      buy: {
        baseCurrency: Currency.CZK,
        targetCurrency: currency,
        rate: rate / units,
        direction: Direction.BUY,
        date: new Date()
      }
    };

    return rates;
  }, {});
}
