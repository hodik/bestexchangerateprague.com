import { CountryCode, Currency } from "../model/types";
import { shutdownBrowser } from "../services/puppeteer-manager";
import { getRatesIn } from "../lib/get-rates-in";
import { existsSync, mkdirSync, writeFileSync } from "fs";
import { findCurrencyByCode } from "../lib/find-currency-by-code";
import { dirname, resolve } from "path";

(async () => {
  // const requestedCountry: string = (process.argv[2] || "").toLowerCase().trim();
  // const countryCode: CountryCode =
  //   CountryCode[requestedCountry as keyof typeof CountryCode];

  const countryCode = CountryCode.CZECH_REPUBLIC;
  const city = process.argv[3];
  const requestedCurrencies = process.argv[4] || "";
  const currencies: Currency[] = requestedCurrencies
    .split(",")
    .map(findCurrencyByCode);
  const targetFile = resolve(process.cwd(), process.argv[5]);
  const outputDir = dirname(targetFile);

  if (!existsSync(outputDir)) {
    // tslint:disable-next-line:no-console
    console.log(`Trying to create ${outputDir}...`);
    mkdirSync(outputDir, {
      recursive: true
    });
  }

  // tslint:disable-next-line:no-console
  console.log(`Scraped data will be written to ${targetFile}`);

  try {
    // tslint:disable-next-line:no-console
    console.log("Scraping data...");
    const ratesInPrague = await getRatesIn(countryCode, city, currencies);

    // tslint:disable-next-line:no-console
    console.log(`Writing data to disk...`);
    const payload = JSON.stringify(ratesInPrague, null, 4);
    writeFileSync(targetFile, payload, "utf-8");

    // tslint:disable-next-line:no-console
    console.log(payload);
    // tslint:disable-next-line:no-console
    console.log(`Successfully saved to ${targetFile}`);
  } catch (err) {
    // tslint:disable-next-line:no-console
    console.error(err);
  } finally {
    await shutdownBrowser();
  }
})();
