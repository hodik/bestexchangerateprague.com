import { CountryCode, ICentralBank, IExchange } from "../model/types";

export const CENTRAL_BANKS_SPREADSHEET_ID =
  "1vBuce7cSN4kGLSvcoRp-HFporZrNBPTck4BDxhCz1Xc";

export const EXCHANGE_SPREADSHEET_IDS = {
  [CountryCode.CZECH_REPUBLIC]: "1rslFR9U-d45TcsMCUre4XcLYJn6YYRE6TNYCSKnm1UQ"
};

export const CENTRAL_BANK_SPREADSHEET_HEADER_TO_FIELDS_MAP: {
  [id in keyof ICentralBank]: string;
} = {
  id: "ID",
  countryCode: "Country Code",
  name: "Name",
  getRatesFuncKey: "getRatesFunctionName",
  url: "URL"
};

export const EXCHANGE_SPREADSHEET_HEADER_TO_FIELDS_MAP: {
  [id in keyof IExchange]: string;
} = {
  id: "ID",
  name: "Name",
  active: "Active",
  address: "Address",
  url: "Website",
  getRatesFuncKey: "GetRatesFunctionName"
};
