import * as firebase from "firebase-admin";
import * as serviceAccount from "./firebase-crendentials.json";

const params = {
  type: serviceAccount.type,
  projectId: serviceAccount.project_id,
  privateKeyId: serviceAccount.private_key_id,
  privateKey: serviceAccount.private_key,
  clientEmail: serviceAccount.client_email,
  clientId: serviceAccount.client_id,
  authUri: serviceAccount.auth_uri,
  tokenUri: serviceAccount.token_uri,
  authProviderX509CertUrl: serviceAccount.auth_provider_x509_cert_url,
  clientC509CertUrl: serviceAccount.client_x509_cert_url
};

export const firebaseApp = firebase.initializeApp({
  credential: firebase.credential.cert(params),
  databaseURL: "https://berp-1.firebaseio.com"
});

(async () => {
  return;
  const db = firebase.app().firestore();
  const exchanges = db.collection("exchanges");
  const pairs = db.collection("pairs");

  // READ: List all documents
  const exchange = (await exchanges.listDocuments())[0];

  // READ: Query documents
  await pairs
    .where("exchange", "==", exchange)
    .where("direction", "==", "sell")
    .get();

  // snapshots.forEach(snapshot => {
  //   console.log(snapshot.data());
  // });

  // CREATE:
  const pairRef = pairs.doc();
  await pairRef.set({
    ccy1: "USD",
    ccy2: "CZK",
    exchange,
    direction: "sell",
    day: new Date(),
    value: 25.3
  });

  // UPDATE
  await pairRef.set(
    {
      ccy1: "RUB"
    },
    {
      merge: true
    }
  );
})();
