import puppeteer, { Browser, Page } from "puppeteer";
import {
  Currency,
  GetRatesFunc,
  PuppeteerBasedGetRatesFunc
} from "../model/types";

let chrome: Browser;

export async function getBrowserPage(): Promise<Page> {
  if (!chrome) {
    chrome = await puppeteer.launch({ args: ["--no-sandbox"] });
  }

  return chrome.newPage();
}

export async function shutdownBrowser(): Promise<void> {
  return chrome.close();
}

export function convertToSimpleFunction(
  func: PuppeteerBasedGetRatesFunc
): GetRatesFunc {
  return async (currencies: Currency[]) =>
    func(await getBrowserPage(), currencies);
}
